import namespace from '@/utils/namespace';

export default namespace( 'auth', {
    getters: [
        'usuario',
        'logged'
    ],
    actions: [
        'login',
        'registro',
        'logout',
        'actualizarPerfil'
    ],
    mutations: [
        'setUsuario',
        'setLogged'
    ]
} );