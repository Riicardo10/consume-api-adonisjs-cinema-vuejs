import Vue    from 'vue'
import App    from '@/App.vue'
import router from '@/routes'

// <vue resource>
    import VueResource from 'vue-resource';
    Vue.use( VueResource );
    Vue.http.options.root = 'http://127.0.0.1:3333/api/v1/';
    // Vue.http.options.root = 'http://192.168.1.67:3333/api/v1/';
    Vue.http.interceptors.push( (request, next) => {
        request.headers.set( 'Authorization', 'Bearer ' + window.localStorage.getItem( '_token' ) );
        // request.headers.set('Authorization', `Bearer ${window.localStorage.getItem('_token')}`);
        next();
    } );
// </vue resource>

// <vuex>
    import Vuex from 'vuex';
    Vue.use( Vuex );
// </vuex>

// <blockui>
    import BlockUI from 'vue-blockui';
    Vue.use( BlockUI );
// <blockui>

// <modulos>
    import globalTypes from '@/types/global';
    import authModule from '@/modules/auth';
    import cinemaModule from '@/modules/cinema';
    import peliculaModule from '@/modules/pelicula';
// </modulos>

// <validate>
    import VeeValidate, {Validator} from 'vee-validate';

    import validatorEs from '@/validator/es';
    import validatorEn from '@/validator/en';
    Validator.localize( 'es', validatorEs );

    Vue.use( VeeValidate );
// </validate>

// <vue table>
    import {ClientTable} from 'vue-tables-2';
    Vue.use( ClientTable, {}, false, 'bootstrap3', 'default' );
// </vue table>

// store.dispatch( globalTypes.actions.cambiarLenguaje, 'en' ).then();

// <almacen datos vuex>
    export const store = new Vuex.Store( {
        state:     {
            procesando: false,
            lenguaje:   'es'
        }, 
        actions:   {
            [globalTypes.actions.cambiarLenguaje]: ( {commit}, lenguaje ) => {
                commit( globalTypes.mutations.setLenguaje, lenguaje );
                switch( lenguaje ) {
                    case 'en':
                        Validator.localize( 'en', validatorEn );
                        break;
                    case 'es':
                        Validator.localize( 'es', validatorEs );
                        break;
                }
            }
        },
        getters:   {
            [globalTypes.getters.procesando]:   (state) => {
                return state.procesando;
            },
            [globalTypes.getters.lenguaje]:     (state) => {
                return state.lenguaje;
            }
        },
        mutations: {
            [globalTypes.mutations.startProceso] (state) {
                state.procesando = true;
            },
            [globalTypes.mutations.stopProceso] (state) {
                state.procesando = false;
            },
            [globalTypes.mutations.setLenguaje] (state, lenguaje) {
                state.lenguaje = lenguaje;
            }
        },
        modules:   {
            authModule,
            cinemaModule,
            peliculaModule
        }
    } );
// </almacen>

// <vue traducciones>

import VueI18n from 'vue-i18n';
Vue.use( VueI18n );
import mensajes from '@/translations';
const i18n = new VueI18n( {
    locale:     store.state.lenguaje,
    messages:   mensajes
} );

// </vue traducciones>

new Vue( {
    el: '#app',
    render: h => h(App),
    store,
    i18n,
    router
} )
