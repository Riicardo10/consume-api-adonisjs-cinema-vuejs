import Vue    from 'vue';
import Router from 'vue-router'
Vue.use( Router );

// <components>

// </components>

// <types>
import authTypes   from '@/types/auth';
import Login       from '@/components/Auth/Login';
import Registro    from '@/components/Auth/Registro';
import Cinema      from '@/components/Cinema/Cinema';
import Pelicula    from '@/components/Pelicula/Pelicula';
// </types>

// <global store>
import {store} from '@/main'
// </global store>

// <router>
const router = new Router( {
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { Auth: false, title: 'Login' },
            beforeEnter: (to, from, next) => {
                if( store.state.authModule.logged ) {
                    next( {path: '/'} );
                }
                else{
                    next();
                }
            }
        },
        {
            path: '/registro',
            name: 'registro',
            component: Registro,
            meta: { Auth: false, title: 'Registro' },
            beforeEnter: (to, from, next) => {
                if( store.state.authModule.logged ) {
                    next( {path: '/'} );
                }
                else{
                    next();
                }
            }
        },
        {
            path: '/',
            name: 'cinemas',
            component: Cinema,
            meta: { Auth: false, title: 'Cines' },
        },
        {
            path: '/cinema/:id',
            name: 'cinema',
            component: Pelicula,
            meta: { Auth: false, title: 'Listado de peliculas' },
        }
    ]
} );
// </router>

// <cambio de ruta>
router.beforeEach( (to, from, next) => {
    document.title = to.meta.title;
    if( to.meta.Auth && !store.state.authModule.logged ) {
        next( {path: '/login'} );
    }
    else{
        if( store.state.authModule.logged ) {
            store.commit(authTypes.mutations.setUsuario);
        }
        next();
    }
    //next();
    //return;
    /*if( to.meta.Auth && !store.state.authModule.logged ) {
        next( {path: '/'} );
    }
    else{
        if( store.state.authModule.logged ) {
            store.commit(authTypes.mutations.setUser);
        }
        next();
    }*/
} );
// <cambio de ruta>

export default router;