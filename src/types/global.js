import namespace from '@/utils/namespace';

export default namespace( 'global', {
    actions: [
        'cambiarLenguaje'
    ],
    getters: [
        'procesando',
        'lenguaje'
    ],
    mutations: [
        'startProceso',
        'stopProceso',
        'setLenguaje'
    ]
} );