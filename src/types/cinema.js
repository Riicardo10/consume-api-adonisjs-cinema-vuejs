import namespace from '@/utils/namespace';

export default namespace( 'cinema', {
    getters: [
        'cinemas',
        'buscar',
        'salas',
        'asientos'
    ],
    actions: [
        'fetchCinemas'
    ],
    mutations: [
        'cinemasRecibidos',
        'setBusqueda',
        'setSalas',
        'setAsientos',
        'limpiarFiltro'
    ]
} );