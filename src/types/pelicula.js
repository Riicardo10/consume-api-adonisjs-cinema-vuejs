import namespace from '@/utils/namespace';

export default namespace( 'pelicula', {
    actions: [
        'fetchPeliculas',
        'fetchGeneros'
    ],
    getters: [
        'peliculas',
        'buscar',
        'filas',
        'asientos',
        'generos',
        'genero',
        'hora'
    ],
    mutations: [
        'peliculasRecibidas',
        'generosRecibidos',
        'setBusqueda',
        'setFilas',
        'setAsientos',
        'setGenero',
        'setHora',
        'limpiarFiltro'
    ]
} );