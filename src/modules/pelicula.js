import peliculaTypes from '@/types/pelicula';
import globalTypes from '@/types/global';
import Vue from 'vue';

const state = {
    cinemaInfo: {},
    generos: [],
    query:   {
        buscar:   '',
        filas:    null,
        asientos: null,
        hora:     null,
        genero:   null
    }
};

const actions = {
    [peliculaTypes.actions.fetchPeliculas]: ( {commit}, cinemaId ) => {
        commit( globalTypes.mutations.startProceso );
        Vue.http.get( 'pelicula/' + cinemaId + '/cinema' )
            .then( (result) => {
                commit( peliculaTypes.mutations.peliculasRecibidas, { apiResponse: result } );
                commit( globalTypes.mutations.stopProceso );
            });
    },
    [peliculaTypes.actions.fetchGeneros]: ( {commit} ) => {
        commit( globalTypes.mutations.startProceso );
        Vue.http.get( 'genero' )
            .then( (result) => {
                commit( peliculaTypes.mutations.generosRecibidos, { apiResponse: result } );
                commit( globalTypes.mutations.stopProceso );
            });
    },

};

const getters = {
    [peliculaTypes.getters.peliculas]:  (state) => {
        let peliculas = state.cinemaInfo.data.presentacion_peliculas;
        if(state.query.buscar) {
            peliculas = peliculas.filter( (pelicula) => {
                return pelicula.pelicula.nombre.toLowerCase().includes( state.query.buscar );
            } );
        }
        if(state.query.filas) {
            peliculas = peliculas.filter( (pelicula) => {
                return pelicula.sala.filas >= state.query.filas;
            } );
        }
        if(state.query.asientos) {
            peliculas = peliculas.filter( (pelicula) => {
                return pelicula.sala.asientos >= state.query.asientos;
            } );
        }
        if(state.query.genero) {
            peliculas = peliculas.filter( (pelicula) => {
                return pelicula.pelicula.generos.some( genero => genero.pivot.genero_id === state.query.genero );
            } );
        }
        if(state.query.hora) {
            peliculas = peliculas.filter( (pelicula) => {
                return pelicula.presentacion_pelicula_horarios.some( presentacion_pelicula_horario => {
                    const hora = presentacion_pelicula_horario.horario.split( ':' );
                    return parseInt( hora[0] ) === state.query.hora;
                } );
            } );
        }
        return peliculas;
    },
    [peliculaTypes.getters.buscar]:     (state) => {
        return state.query.buscar;
    },
    [peliculaTypes.getters.filas]:      (state) => {
        return state.query.filas;
    },
    [peliculaTypes.getters.asientos]:   (state) => {
        return state.query.asientos;
    },
    [peliculaTypes.getters.generos]:    (state) => {
        return state.generos;
    },
    [peliculaTypes.getters.genero]:     (state) => {
        return state.query.genero;
    },
    [peliculaTypes.getters.hora]:       (state) => {
        return state.query.hora;
    }
};

const mutations = {
    [peliculaTypes.mutations.peliculasRecibidas]:   ( state, {apiResponse} ) => {
        state.cinemaInfo = apiResponse.data;
    },
    [peliculaTypes.mutations.generosRecibidos]:     ( state, {apiResponse} ) => {
        state.generos = apiResponse.data
    },
    [peliculaTypes.mutations.setBusqueda]:          ( state, query ) => {
        state.query.buscar = query;
    },
    [peliculaTypes.mutations.setFilas]:             ( state, filas ) => {
        state.query.filas = filas;
    },
    [peliculaTypes.mutations.setGenero]:            ( state, genero ) => {
        state.query.genero = genero;
    },
    [peliculaTypes.mutations.setAsientos]:          ( state, asientos ) => {
        state.query.asientos = asientos;
    },
    [peliculaTypes.mutations.setHora]:              ( state, hora ) => {
        state.query.hora = hora;
    },
    [peliculaTypes.mutations.limpiarFiltro]:        ( state ) => {
        state.query = {
            buscar:   '',
            filas:    null,
            asientos: null,
            hora:     null,
            genero:   null           
        }
    }
};

export default {
    state,
    getters,
    mutations,
    actions
}