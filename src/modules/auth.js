import globalTypes from '@/types/global';
import authTypes from '@/types/auth';
import Vue from 'vue';

const state = {
    usuario: null,
    logged: !!window.localStorage.getItem( '_token' )
};

const actions = {
    [authTypes.actions.login]: ( {commit}, usuarioInput ) => {
        commit( globalTypes.mutations.startProceso );
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'login', {data: usuarioInput} )
                .then( (result) => {
                    const tokensito = result.body.token;
                    window.localStorage.setItem( '_token', tokensito );
                    commit( authTypes.mutations.setUsuario );
                    resolve(result);   
                
                })
                .catch( (err) => {
                    reject( err );
                })
                .finally( () => {
                    commit( globalTypes.mutations.stopProceso );
                } );
        })
        
    }, 
    [authTypes.actions.registro]: ( {commit}, usuarioInput ) => {
        commit(globalTypes.mutations.startProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'registro', {data: usuarioInput} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(globalTypes.mutations.stopProceso);
                } );
        } );
    },
    [authTypes.actions.actualizaPerfil]: ( {commit}, usuarioInput ) => {
        commit( globalTypes.mutations.startProceso )
        return new Promise( (resolve, reject) => {
            Vue.http.put( 'perfil', {usuario: usuarioInput} )
                .then( (result) => {
                    window.localStorage.setItem( '_token', result.body.token );
                    commit( authTypes.mutations.setUsuario );
                    resolve( usuario );
                })
                .catch( (err) => {
                    reject( err );
                })
                .finally( () => {
                    commit( globalTypes.mutations.stopProceso );
                } );
        })
        
    }, 
    [authTypes.actions.logout]: ( {commit} ) => {
        window.localStorage.removeItem( '_token' );
        commit( authTypes.mutations.setUsuario );
    }
};

const getters = {
    [authTypes.getters.usuario]: (state) => {
        return state.usuario;
    },
    [authTypes.getters.logged]: (state) => {
        return state.logged;
    }
};

const mutations = {
    [authTypes.mutations.setUsuario]:    (state) => {
        if( window.localStorage.getItem( '_token' ) ) {
            const token = window.localStorage.getItem( '_token' );
            const jwtDecode = require( 'jwt-decode' );
            state.usuario = jwtDecode( token );
            state.logged = true;
        }
        else {
            state.logged    = false;
            state.usuario   = null;   
        }
    },
    [authTypes.mutations.setLogged]:     (state, logged) => {
        state.logged = logged;
    }
};

export default {
    state,
    actions,
    getters, 
    mutations
}