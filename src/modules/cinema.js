import cinemaTypes from '@/types/cinema';
import globalTypes from '@/types/global';
import Vue from 'vue';

const state = {
    cinemas: [],
    count: 0,
    query:   {
        buscar:   '',
        salas:    null,
        asientos: null
    }
};

const actions = {
    [cinemaTypes.actions.fetchCinemas]: ( {commit} ) => {
        commit( globalTypes.mutations.startProceso );
        Vue.http.get( 'cinema' )
            .then( (result) => {
                commit( cinemaTypes.mutations.cinemasRecibidos, { apiResponse: result } );
                commit( globalTypes.mutations.stopProceso );
            });
    }
};

const getters = {
    [cinemaTypes.getters.buscar]: (state) => {
        return state.query.buscar;
    },
    [cinemaTypes.getters.salas]: (state) => {
        return state.query.salas;
    },
    [cinemaTypes.getters.asientos]: (state) => {
        return state.query.asientos;
    },
    [cinemaTypes.getters.cinemas]: (state) => {
        let cinemas = state.cinemas;
        if( state.query.buscar ) {
            cinemas = cinemas.filter( (cinema) => {
                return cinema.nombre.toLowerCase().includes( state.query.buscar );
            } );
        }
        if( state.query.salas ) {
            cinemas = cinemas.filter( (cinema) => {
                return cinema.__meta__.salas_count == state.query.salas;
            } );
        }
        if( state.query.asientos ) {
            cinemas = cinemas.filter( (cinema) => {
                return cinema.asientos >= state.query.asientos;
            } );
        }
        state.count = cinemas.length;
        return  {data: cinemas, total: state.count };
    }
};

const mutations = {
    [cinemaTypes.mutations.cinemasRecibidos]:   ( state, {apiResponse} ) => {
        state.cinemas = apiResponse.data.data;
    },
    [cinemaTypes.mutations.setBusqueda]:        ( state, query ) => {
        state.query.buscar = query;
    },
    [cinemaTypes.mutations.setSalas]:           ( state, salas ) => {
        state.query.salas = salas;
    },
    [cinemaTypes.mutations.setAsientos]:        ( state, asientos ) => {
        state.query.asientos = asientos;
    },
    [cinemaTypes.mutations.limpiarFiltro]:      ( state ) => {
        state.query = {
            buscar:     '',
            asientos:   null,
            salas:      null
        }
    },
};

export default {
    state,
    actions,
    getters, 
    mutations
}